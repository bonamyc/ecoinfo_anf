#define MAX 10
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
 
int * tableau = NULL ;
 


void bubble_sort(int list[], long n)
{
  long c, d, t;
 
  for (c = 0 ; c < ( n - 1 ); c++)
  {
    printf ("Etape %ld\n",c);
    for (d = 0 ; d < n - c - 1; d++)
    {
      if (list[d] > list[d+1])
      {
        /* Swapping */
        t         = list[d];
        list[d]   = list[d+1];
        list[d+1] = t;
      }
    }
  }
}



void creer_tableau()
{
  int i;

  for (i=0;i<MAX;i++) 
    {
      tableau = realloc (tableau, (i+1) * sizeof(int) );
      tableau[i]=rand()*i;
      printf("--> Allocation tableau case : %d \n",i);
    }
}
 
int main()
{ int i;
  
  creer_tableau();
  for (i=0;i<MAX;i++) printf("%d\n",tableau[i]);

  sleep(3);
  printf("Debut du tri\n");
 
  bubble_sort(tableau, MAX);
 
  printf("Fin du tri\n");

  for (i=0;i<MAX;i++) printf("%d\n",tableau[i]);
 

  free(tableau);
  return 0;
}

