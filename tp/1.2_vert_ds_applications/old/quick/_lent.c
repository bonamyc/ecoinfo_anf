#define MAX 300000

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
 
int * tableau = NULL ;
 


void quick_sort (int *a, int n) {
    if (n < 2)
        return;
    int p = a[n / 2];
    int *l = a;
    int *r = a + n - 1;
    while (l <= r) {
        if (*l < p) {
            l++;
        }
        else if (*r > p) {
            r--;
        }
        else {
            int t = *l;
            *l = *r;
            *r = t;
            l++;
            r--;
        }
    }
    printf ("Etape %d\n",n);

    quick_sort(a, r - a + 1);
    quick_sort(l, a + n - l);
}
 


void creer_tableau()
{
  int i;

  for (i=0;i<MAX;i++) 
    {
      tableau = realloc (tableau, (i+1) * sizeof(int) );
      tableau[i]=rand()*i;
      printf("--> Allocation tableau case : %d \n",i);
    }
}
 
int main()
{ int i;
  
  creer_tableau();
  for (i=0;i<MAX;i++) printf("%d\n",tableau[i]);

  sleep(3);
  printf("Debut du tri\n");
 
  quick_sort(tableau, MAX);
 
  printf("Fin du tri\n");

  for (i=0;i<MAX;i++) printf("%d\n",tableau[i]);
 

  free(tableau);
  return 0;
}

