#!/bin/bash
# test stress

date=`date +%s`
echo "date = $date --> stress for $1 seconds" >> logs 
time=$(($1 - 3))
(stress -t $time -c 10 -i 10 -m 10 -d 10 | cat ) >> logs &
sleep $1
(killall -q stress | cat ) > /dev/null
date=`date +%s`
