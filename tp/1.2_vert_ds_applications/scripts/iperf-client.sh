#!/bin/bash
# test iperf client

date=`date +%s`
echo "date = $date --> iperf-client for $1 seconds" 
echo "START client :->  $date"   
time=$(($1 - 2))
time=$(($time / 2))
machine=$2
(iperf -c $machine -i 20 -t $time | cat) >> logs &
sleep $1
(killall -q iperf | cat) > /dev/null
date=`date +%s`
echo "STOP client :->  $date" 

