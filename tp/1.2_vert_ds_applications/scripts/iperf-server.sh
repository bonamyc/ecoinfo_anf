#!/bin/bash
# test iperf server

date=`date +%s`
echo "date = $date --> iperf-server for $1 seconds" >> logs 
(iperf -s -i 20 | cat ) >> logs &
sleep $1
(killall -q iperf | cat ) > /dev/null
date=`date +%s`
