#!/bin/bash
DO_ALL=1

# Setup script
# set -eux to fail fast
set -eux ;

BASE_PKG="procps psmisc vim wget curl htop linux-tools-common linux-tools-generic stress hdparm iperf snmp nload"
BUILD_PKG="make gcc g++ gfortran python3 python3-dev python3-pip"
apt-get update
apt-get install -y --no-install-recommends $BASE_PKG $BUILD_PKG


if [ "$DO_ALL" -eq "1" ] ; then

# Julia
JL=julia-1.1.1-linux-x86_64.tar.gz
wget --no-check-certificate -O $JL https://julialang-s3.julialang.org/bin/linux/x64/1.1/$JL
tar xvfz $JL && rm -rf /opt/julia-1.1.1 && mv julia-1.1.1 /opt/ && rm $JL
echo "Julia installed at /opt/julia-1.1.1/"

fi

# Python:
pip3 install numpy
pip3 install setuptools # separate install needed
pip3 install transonic pythran
pip3 install numba matplotlib


# Docker:
apt-get remove docker docker-engine docker.io containerd runc
apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
apt-get update
apt-get install -y docker-ce docker-ce-cli containerd.io

echo "setup done."

