#!/bin/bash
# Needs sudo

FREQ="2.0GHz" # nominal
echo "set CPU frequency to $FREQ ..."

# set fixed frequency with governor 'performance'
cpupower -c all frequency-set -d $FREQ -u $FREQ -g performance 
cpupower -c all frequency-info

# set perf bias
cpupower -c all set -b 0
cpupower -c all info

