#!/bin/bash
# Needs sudo

cpupower -c all frequency-set -d 800MHz -u 3.6GHz -g powersave 
cpupower -c all frequency-info

# set perf bias
cpupower -c all set -b 6
cpupower -c all info

