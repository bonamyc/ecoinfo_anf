#!/bin/bash

# Lecture de la consommation (W) d'une prise du PDU
# watch -n 1 snmp_pdu_get.sh

# adresse du PDU (locale)
PDU_ADDR=192.168.1.10

# "Community Name" accessible dans l'interface web du PDU via Configuration->Network->SNMPv1->Access Control
PDU_COMMUNITY_NAME=public

# Numéro de la prise
PLUG_PORT=9

snmpget -v1 -c ${PDU_COMMUNITY_NAME} ${PDU_ADDR} 1.3.6.1.4.1.318.1.1.26.9.4.3.1.7.${PLUG_PORT}

