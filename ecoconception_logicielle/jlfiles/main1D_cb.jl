
## Tri Diagonal Matrix Algorithm(a.k.a Thomas algorithm) solver
function TDMAsolver(a, b, c, d)
    """
    TDMA solver, a b c d can be NumPy array type or Python list type.
    refer to http://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm
    """
    (nf,) = size(a)     # number of equations
    #ac, bc, cc, dc = map(np.array, (a, b, c, d))     # copy the array
    for it in 2:nf
        mc = a[it] / b[it-1]
        b[it] = b[it] - mc * c[it - 1]
        d[it] = d[it] - mc * d[it - 1]
    end
    xc = a
    xc[nf] = d[nf]/b[nf]

    for il in nf-1:-1:1
        xc[il] = (d[il] - c[il] * xc[il + 1]) / b[il]
    end
    return xc
#    clear!(:bc, :cc, :dc)  # delete variables from memory
end

function RMS_E(X1, X2)
    (sizeX1,) = size(X1)
    return sqrt(sum((X1 - X2).^2) / sizeX1)
end

start = time_ns()
#
#
#     INPUT PARAMETERS
#
# Domain length
Lx = 1.0

# mesh cells in x-direction
M = 2048 * 2048

# Grid sizes
dx = Lx / (M - 1)

# Pressure gradient
dpdx = 1.0

# Reynolds number: Re = U L / nu
Re = 1.0

# Diffusivity
Gamma = 1.0 / Re

# Velocity
c0 = 0.1

# Time step:
dt = 0.1

# Configuration
case = "diffusion"
#case = "convection"

#
#     INITIALISATION
#

#
x = range(-Lx / 2, Lx / 2, length = M)

# Initial velocity vector field
uo = zeros(M)

if (case == "convection")
    # Pressure gradient
    dpdx = 0.0
    # Velocity
    c0 = 0.1
    # Diffusivity
    Gamma = 0
    @. uo = 1.0 / (sqrt(2 * 0.05) * 1pi) * exp(-x^2 / (2 * 0.05^2))
    # Time step:
    dt = 0.1
    # Maximum number of iterations
    maxIter = 1000  # 1.5/dt
elseif (case == "diffusion")
    # Pressure gradient
    dpdx = 1.0
    # Velocity
    c0 = 0.0
    # Diffusivity
    Gamma = 1.0 / Re
    # Time step:
    dt = 0.1
    # Maximum number of iterations
    maxIter = 1000
end

# Precision
eps = 1e-9

#
# Print initial conditions:
print("case : ",case, "\n");
print("M  : ", M, "\n");
print("dx : ", dx, "\n");
print("dt : ", dt, "\n");
print("eps: ", eps, "\n");


# boundary conditions
BCNorth = 0
BCSouth = 0

# Initialisation of variables
u    = zeros(M)
uold = zeros(M)

Aua = zeros(M)
Aub = zeros(M)
Auc = zeros(M)
Aud = zeros(M)


uold = copy(uo)
u    = copy(uo)

# Min step check:
last_rms = 1.0
min_step = eps / 10.0

# First iteration
k = 0
while k < maxIter
    global uold, u, k, last_rms
    #Predictor step (input : c0, Gamma, dx, dt, uold, dpdx; output : Aua, Aub, Auc, Aud)
    for j in 2:M-1 #only internal nodes
        #
        # Convection fluxes
        #
        Fe = c0
        Fw = c0
        #
        # Diffusion fluxes
        #
        De=Gamma / dx
        Dw=Gamma / dx
        #
        # Compute system coefficients
        #
        aE = De + max(-Fe, 0)
        aW = Dw + max( Fw, 0)
        aP = aE + aW + Fe - Fw
        #
        # Assemble matrix coefficients
        #
        Aua[j] = -aW
        Aub[j] = dx / dt + aP
        Auc[j] = -aE
        Aud[j] = dx / dt * uold[j] + dpdx * dx
    end

    # Boundary conditions
    # North boundary
    if (BCNorth == 0) # Dirichlet
        u[M] = 0
        Aub[M] = 1
        Aud[M] = 0
    elseif (BCNorth == 1) # Neumann
        u[M] = u[M - 1]
        Aua[M] = -1
        Aub[M] = 1
        Aud[M] = 0
    end
    # South boundary
    if (BCSouth == 0) # Dirichlet
        u[1] = 0
        Aub[1] = 1
        Aud[1] = 0
    elseif (BCSouth == 1) # Neumann
        u[1] = u[2]
        Auc[1] = -1
        Aub[1] = 1
        Aud[1] = 0
    end

    # Solve algebraic system
    u = TDMAsolver(Aua, Aub, Auc, Aud)

    # Compute rms error at the end of each time step
    rms_u = RMS_E(u, uold)

    if (rms_u < eps)
        print("The simulation has converged in ", k, " iterations\n")
        break
    elseif (abs(last_rms - rms_u) < min_step)
        print("The simulation convergence is limited to ", rms_u, " in ", k, " iterations\n")
        break
    else
     	print("k=", k, ": RMS U =", rms_u, "\n")
    end
    uold = copy(u)
    k = k + 1
    last_rms = rms_u
end

endtime = time_ns()
print("Time in seconds = ", (endtime - start) / 1.0e9, "\n")  # Time in seconds, e.g. 5.38091952400282

print("result samples:\n")
for idx in 0:trunc(Int32, M/10):M
    print("u[", idx, "] = ", u[idx + 1], "\n");
end

