
## Tri Diagonal Matrix Algorithm(a.k.a Thomas algorithm) solver
function TDMAsolver(a, b, c, d, nf, xc, tmp)
    """
    TDMA solver, a b c d can be NumPy array type or Python list type.
    refer to http://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm
    """
    # use outputs for temporary arrays:
    bc = xc
    dc = tmp

    # (nf,) = size(b)  # number of equations
    bc .= b     # copy the array (in-place)
    dc .= d     # copy the array (in-place)

    @inbounds for it in 2:nf
        mc = a[it - 1] / bc[it - 1]
        bc[it] = bc[it] - mc *  c[it - 1]
        dc[it] = dc[it] - mc * dc[it - 1]
    end
    @inbounds xc[nf] = dc[nf] / bc[nf]

    @inbounds for il in nf-1:-1:1
        xc[il] = (dc[il] - c[il] * xc[il + 1]) / bc[il]
    end
end

function RMS_E(X1, X2, sizeX)
    # manual loop is faster (4.9s vs 5.2s)
    tot = 0.0
    @inbounds for i in 1:sizeX
        tot += (X1[i] - X2[i])^2
    end
    return sqrt(tot / sizeX)
    # update X2 in-place
    #    @. X2 = (X1 - X2)^2
    #    return sqrt(sum(X2) / sizeX)
end

function main()
    start = time_ns()
    #
    #
    #     INPUT PARAMETERS
    #
    # Domain length
    Lx = 1.0

    # mesh cells in x-direction
    M = 2048 * 2048

    # Grid sizes
    dx = Lx / (M - 1)

    # Pressure gradient
    dpdx = 1.0

    # Reynolds number: Re = U L / nu
    Re = 1.0

    # Diffusivity
    Gamma = 1.0 / Re

    # Velocity
    c0 = 0.1

    # Time step:
    dt = 0.1

    # Configuration
    case = "diffusion"
    #case = "convection"

    #
    #     INITIALISATION
    #


    # Initial velocity vector field
    uo = zeros(M)

    if (case == "convection")
        x = range(-Lx / 2, Lx / 2, length = M)
        # Pressure gradient
        dpdx = 0.0
        # Velocity
        c0 = 0.1
        # Diffusivity
        Gamma = 0
        @. uo = 1.0 / (sqrt(2 * 0.05) * 1pi) * exp(-x^2 / (2 * 0.05^2))
        # Time step:
        dt = 0.1
        # Maximum number of iterations
        maxIter = 1000  # 1.5/dt
    elseif (case == "diffusion")
        # Pressure gradient
        dpdx = 1.0
        # Velocity
        c0 = 0.0
        # Diffusivity
        Gamma = 1.0 / Re
        # Time step:
        dt = 0.1
        # Maximum number of iterations
        maxIter = 1000
    end

    # Precision
    eps = 1e-9

    #
    # Print initial conditions:
    print("case : ",case, "\n");
    print("M  : ", M, "\n");
    print("dx : ", dx, "\n");
    print("dt : ", dt, "\n");
    print("eps: ", eps, "\n");


    # boundary conditions
    BCNorth = 0
    BCSouth = 0

    # Initialisation of variables
    u    = zeros(M)
    uold = zeros(M)

    Aua = zeros(M - 1)
    Aub = zeros(M)
    Auc = zeros(M - 1)
    Aud = zeros(M)


    uold = copy(uo)
    u    = copy(uo)

    #
    # Convection fluxes
    #
    Fe = c0
    Fw = c0
    #
    # Diffusion fluxes
    #
    De = Gamma / dx
    Dw = Gamma / dx
    #
    # Compute system coefficients
    #
    aE = De + max(-Fe, 0)
    aW = Dw + max( Fw, 0)
    aP = aE + aW + Fe - Fw
    #
    # Assemble matrix coefficients
    #
    Aua[1 : M - 1] .= -aW
    Aub[2 : M - 1] .= dx / dt + aP
    Auc[1 : M - 1] .= -aE

    # Boundary conditions

    # North boundary
    if (BCNorth == 0) # Dirichlet
        Aub[M] = 1
        Aud[M] = 0
    elseif (BCNorth == 1) # Neumann
        Aua[M - 1] = -1
        Aub[M] = 1
        Aud[M] = 0
    end
    # South boundary
    if (BCSouth == 0) # Dirichlet
        Aub[1] = 1
        Aud[1] = 0
    elseif (BCSouth == 1) # Neumann
        Auc[1] = -1
        Aub[1] = 1
        Aud[1] = 0
    end

    # Min step check:
    last_rms = 1.0
    min_step = eps / 10.0

    # Preallocate 1 temporary array
    tmp = zeros(M)

    # First iteration
    k = 0
    while k < maxIter
        # Predictor step
        #
        # Convection fluxes
        #
        # in-place:
        @. Aud = dx / dt * uold + dpdx * dx

        # Boundary conditions
        # North boundary
        if (BCNorth == 0) # Dirichlet
            u[M] = 0
            Aud[M] = 0
        elseif (BCNorth == 1) # Neumann
            u[M] = u[M - 1]
            Aud[M] = 0
        end
        # South boundary
        if (BCSouth == 0) # Dirichlet
            u[1] = 0
            Aud[1] = 0
        elseif (BCSouth == 1) # Neumann
            u[1] = u[2]
            Aud[1] = 0
        end

        # Solve algebraic system
        # give 2 extra arrays (outputs) for preallocation
        TDMAsolver(Aua, Aub, Auc, Aud, M, u, tmp)

        # Compute rms error at the end of each time step
        rms_u = RMS_E(u, uold, M)  # uold is modified in function (tmp)

        if (rms_u < eps)
            print("The simulation has converged in ", k, " iterations\n")
            break
        elseif (abs(last_rms - rms_u) < min_step)
            print("The simulation convergence is limited to ", rms_u, " in ", k, " iterations\n")
            break
        else
     	    print("k=", k, ": RMS U =", rms_u, "\n")
        end
        # in-place:
        uold .= u
        k = k + 1
        last_rms = rms_u
    end

    endtime = time_ns()
    print("Time in seconds = ", (endtime - start) / 1.0e9, "\n")  # Time in seconds, e.g. 5.38091952400282

    print("result samples:\n")
    for idx in 0:trunc(Int32, M/10):M
        print("u[", idx, "] = ", u[idx + 1], "\n");
    end

end

main()
