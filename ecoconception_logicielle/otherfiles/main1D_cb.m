clear all;
tic
%
%
%     INPUT PARAMETERS
%
% Domain length
Lx = 1.0;

% mesh cells in x-direction
M = 2048*2048;

% Grid sizes
dx = Lx/(M-1);

% Pressure gradient
dpdx = 1.0;

% Reynolds number: Re = U L / nu
Re = 1.0;

% Diffusivity
Gamma = 1.0/Re;

% Velocity
c0 = 0.1;

% Time step:
dt = 0.1;

% Configuration
casee = 0; %'diffusion';
%casee = 1 % 'convection';

%
%     INITIALISATION
%

%
uo = zeros(M,1);
% Initial velocity vector field
if (casee==1)%'convection'
    x = linspace(-Lx/2, Lx/2, M);
    % Pressure gradient
    dpdx = 0.0;
    % Velocity
    c0 = 0.1;
    % Diffusivity
    Gamma = 0;
    uo=1.0/(sqrt(2*0.05)*pi)*exp(-x.^2/(2*0.05^2));
    % Time step:
    dt = 0.1;
    % Maximum number of iterations
    maxIter = 1000;%1.5/dt
elseif (casee==0)%'diffusion')
    % Pressure gradient
    dpdx = 1.0;
    % Velocity
    c0 = 0.0;
    % Diffusivity
    Gamma = 1.0/Re;
    % Time step:
    dt = 0.1;
    % Maximum number of iterations
    maxIter = 1000;
end
% boundary conditions
BCNorth = 0;
BCSouth = 0;

% Precision
eps = 1e-9;

% Initialisation of variables
u = zeros(M,1);
uold = zeros(M,1);

Aua = zeros(M-1,1);
Aub = zeros(M,1);
Auc = zeros(M-1,1);
Aud = zeros(M,1);


uold = uo;
u    = uo;
        %
        % Convection fluxes
        %
        Fe = c0;
        Fw = c0;
        %
        % Diffusion fluxes
        %
        De=Gamma/dx;
        Dw=Gamma/dx;
        %
        % Compute system coefficients
        %
        aE = De + max(-Fe,0);
        aW = Dw + max( Fw,0);
%        aP = De + Dw + max( Fe,0) + max(-Fw,0)
        aP = aE + aW + Fe - Fw;
        %
        % Assemble matrix coefficients
        %
        Aua(1:M-1) = -aW;
        Aub(2:M-1) = dx/dt + aP;
        Auc(1:M-1) = -aE;
% First iteration
k = 0;
while k < maxIter
    %Predictor step (input : c0, Gamma, dx, dt, uold, dpdx; output : Aua, Aub, Auc, Aud)
        Aud(:) = dx/dt * uold(:) + dpdx * dx;
   % Boundary conditions

    % North boundary
    if (BCNorth == 0) % Dirichlet
        u(M) = 0;
        Aub(M) = 1;
        Aud(M) = 0;
    elseif (BCNorth == 1) % Neumann
        u(M) = u(M-1);
        Aua(M) = -1;
        Aub(M) = 1;
        Aud(M) = 0;
    end
    % South boundary
    if (BCSouth == 0) % Dirichlet
        u(1) = 0;
        Aub(1) = 1;
        Aud(1) = 0;
    elseif (BCSouth == 1) % Neumann
        u(1) = u(2);
        Auc(1) = -1;
        Aub(1) = 1;
        Aud(1) = 0;
    end
    % Solve algebraic system
    [u] = TDMAsolver(Aua, Aub, Auc, Aud);
    % Compute rms error at the end of each time step
    [rms_u] = RMS_E(u, uold);
    if (rms_u < eps)
        disp(['The simulation as converged in ', num2str(k),'iterations']);
        break
    else
     	disp(['k=',num2str(k),': RMS U =', num2str(rms_u)]);
    end
    uold = u;
    k = k + 1;
end
toc

function [xc] = TDMAsolver(a, b, c, d)
    % Tri Diagonal Matrix Algorithm(a.k.a Thomas algorithm) solver
    %TDMA solver, a b c d can be NumPy array type or Python list type.
    %refer to http://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm
    %
    bc = b;
    dc = d;
    nf = length(b);     % number of equations
    %ac, bc, cc, dc = map(np.array, (a, b, c, d))     % copy the array
    for it=2:nf
        mc = a(it-1)/bc(it-1);
        bc(it) = bc(it) - mc*c(it-1);
        dc(it) = dc(it) - mc*dc(it-1);
    end
    xc = bc;
    xc(nf) = dc(nf)/bc(nf);

    for il=nf-1:-1:1
        xc(il) = (dc(il)-c(il)*xc(il+1))/bc(il);
    end
    clear bc
    clear dc
end


function [rms] = RMS_E(X1, X2)
    sizeX1 = length(X1);
    rms = sqrt(sum((X1 - X2).^2)/sizeX1);
end
