import numpy as np
import math
from timeit import default_timer as timer

from numba import jit

## Tri Diagonal Matrix Algorithm(a.k.a Thomas algorithm) solver
@jit(nopython=True, cache=True, fastmath=False)
def TDMAsolver(a, b, c, d):
    """
    TDMA solver, a b c d can be NumPy array type or Python list type.
    refer to http://en.wikipedia.org/wiki/Tridiagonal_matrix_algorithm
    """
    nf = len(b)    # number of equations
    bc = b.copy()  # copy the array
    dc = d.copy()  # copy the array

    for it in range(1, nf):
        mc = a[it - 1] / bc[it - 1]
        bc[it] = bc[it] - mc *  c[it - 1]
        dc[it] = dc[it] - mc * dc[it - 1]

    xc = bc
    xc[-1] = dc[-1] / bc[-1]

    for il in range(nf - 2, -1, -1):
        xc[il] = (dc[il] - c[il] * xc[il + 1]) / bc[il]

    return xc

@jit(nopython=True, cache=True)
def RMS_E(X1, X2):
    rms = math.sqrt(((X1 - X2) ** 2).sum() / len(X1))
    return rms

start = timer()
#
#
#     INPUT PARAMETERS
#
# Domain length
Lx = 1.0

# mesh cells in x-direction
M = 2048 * 2048

# Grid sizes
dx = Lx / (M - 1)

# Pressure gradient
dpdx = 1.0

# Reynolds number: Re = U L / nu
Re = 1.0

# Diffusivity
Gamma = 1.0 / Re

# Velocity
c0 = 0.1

# Time step:
dt = 0.1

# Configuration
case = "diffusion"
#case = "convection"

#
#     INITIALISATION
#


# Initial velocity vector field
uo = np.zeros([M], dtype=np.float)

if (case == "convection"):
    x = np.linspace(-Lx / 2, Lx / 2, M)
    # Pressure gradient
    dpdx = 0.0
    # Velocity
    c0 = 0.1
    # Diffusivity
    Gamma = 0
    uo = 1.0 / (np.sqrt(2 * 0.05) * np.pi) * np.exp(-x ** 2 / (2 * 0.05 ** 2))
    # Time step:
    dt = 0.1
    # Maximum number of iterations
    maxIter = 1000  # 1.5/dt
elif (case == "diffusion"):
    # Pressure gradient
    dpdx = 1.0
    # Velocity
    c0 = 0.0
    # Diffusivity
    Gamma = 1.0 / Re
    # Time step:
    dt = 0.1
    # Maximum number of iterations
    maxIter = 1000

# Precision
eps = 1e-9

#
# Print initial conditions:
print("case : ",case);
print("M  : ", M);
print("dx : ", dx);
print("dt : ", dt);
print("eps: ", eps);


# boundary conditions
BCNorth = 0
BCSouth = 0

# Initialisation of variables
u    = np.zeros([M], dtype=np.float)
uold = np.zeros([M], dtype=np.float)

Aua = np.zeros([M - 1], dtype=np.float)
Aub = np.zeros([M], dtype=np.float)
Auc = np.zeros([M - 1], dtype=np.float)
Aud = np.zeros([M], dtype=np.float)


uold = uo
u    = uo

#
# Convection fluxes
#
Fe = c0
Fw = c0
#
# Diffusion fluxes
#
De = Gamma / dx
Dw = Gamma / dx
#
# Compute system coefficients
#
aE = De + max(-Fe, 0)
aW = Dw + max( Fw, 0)
aP = aE + aW + Fe - Fw
#
# Assemble matrix coefficients
#
Aua[:]         = -aW
Aub[1 : M - 1] = dx / dt + aP
Auc[:]         = -aE

# Boundary conditions

# North boundary
if (BCNorth == 0):  # Dirichlet
    Aub[M - 1] = 1
    Aud[M - 1] = 0
elif (BCNorth == 1):  # Neumann
    Aua[M - 2] = -1
    Aub[M - 1] = 1
    Aud[M - 1] = 0
# South boundary
if (BCSouth == 0):  # Dirichlet
    Aub[0] = 1
    Aud[0] = 0
elif (BCSouth == 1):  # Neumann
    Auc[0] = -1
    Aub[0] = 1
    Aud[0] = 0

# Min step check:
last_rms = 1.0
min_step = eps / 10.0

# First iteration
k = int(0)
while k < maxIter:
    # Predictor step
    #
    # Convection fluxes
    #
    Aud[:] = dx / dt * uold[:] + dpdx * dx

    # Boundary conditions
    # North boundary
    if (BCNorth == 0):  # Dirichlet
        u[M - 1] = 0
        Aud[M - 1] = 0
    elif (BCNorth == 1):  # Neumann
        u[M - 1] = u[M - 2]
        Aud[M - 1] = 0
    # South boundary
    if (BCSouth == 0):  # Dirichlet
        u[0] = 0
        Aud[0] = 0
    elif (BCSouth == 1):  # Neumann
        u[0] = u[1]
        Aud[0] = 0

    # Solve algebraic system
    u = TDMAsolver(Aua, Aub, Auc, Aud)

    # Compute rms error at the end of each time step
    rms_u = RMS_E(u, uold)

    if (rms_u < eps):
        print("The simulation has converged in ", k, " iterations")
        break
    elif (abs(last_rms - rms_u) < min_step):
        print("The simulation convergence is limited to ", rms_u, " in ", k, " iterations\n")
        break
    else:
        print("k=", k, ": RMS U =", rms_u)
    uold = u
    k = k + 1
    last_rms = rms_u

endtime = timer()
print("Time in seconds = ", endtime - start)  # Time in seconds, e.g. 5.38091952400282

print("result samples:")
for idx in range(0, M, int(M / 10)):
    print("u[", idx, "] = ", u[idx]);

